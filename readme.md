# RO-Crate to geoJSON conversion tool
### Overview
This nodejs tool provides a method for converting an RO-Crate into geoJSON format for use with geographical tools such as those used on the TLCMap project.

The tool should be highly configurable as there are many different ways an RO-Crate can express its contents, but will initially just create a point for each GeoCoordinates object.

### How to use
Run the following commands:
```
node cratetojson -d samples
```
Where samples is the relative file directory containing ro-crate-metadata.json

### About
Created by Benjamin McDonnell for UTS as part of the TLCMap project