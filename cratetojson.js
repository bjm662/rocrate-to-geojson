const program = require('commander');
const fs = require('fs-extra');
const path = require('path');
const ROCrate = require('ro-crate').ROCrate;
const GeoJSON = require('geojson');

program
  .version("0.0.1")
  .description(
    "Reads an RO-Crate and produces a geoJSON file to describe its geographical data"
  )
  .option("-d,  --crateDir [crateDir]", "Root directory of the crate to convert. This is the directory containing 'ro-crate-metadata.json'.")
  .option("-c, --config [config]", "Config file for additional configurations")
  .action((d) => {crateDir = d})
program.parse(process.argv);

if (program.crateDir !== undefined) {
    //grab the metadata file from this location
    const crate = main(program.crateDir);
}

if (program.config !== undefined) {
    //grab the config file
}

async function getCrate(crateDir) {
    const crate = new ROCrate( JSON.parse( await fs.readFile( path.join(crateDir, "ro-crate-metadata.json") ) ) );
    return crate;
}

async function getGeoData(crate) {
    const graph = crate.getGraph();
    var arr = [];
    for (let item of graph) {
        if (item['@type'] === 'Geocoordinates') arr.push(item);
    }
    return arr;
}

async function makeGeoJson(data) {
    const geo = GeoJSON.parse(data, {Point: ['latitude','longitude']});
    return geo;
}

async function writeToFile(geoJson) {
    fs.writeFile("./output.geojson", JSON.stringify(geoJson, null, 2));
}

async function main(crateDir, config=null) {
    const crate = await getCrate(crateDir);
    const data = await getGeoData(crate);
    const geoJson = await makeGeoJson(data);
    const file = await writeToFile(geoJson);
}